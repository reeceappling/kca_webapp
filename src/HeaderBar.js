import React from 'react';
import logo from './logo.png';

export default class HeaderBar extends React.Component {
	constructor(props){
		super(props);
		this.changePage = this.props.bodyChangeFunc;
		this.state = {navBarOptions:this.props.navOpts};
	}
	render(){return(
		<div className="headerBar topOrBot">
			<div className="headerContent">
				<NavBar bodyChangeFunc={this.changePage} navOpts={this.state.navBarOptions}/>
				<img src={logo} className="App-logo" alt="logo" />
			</div>
		</div>
	);}
}

//Navigation bar
class NavBar extends React.Component {
	constructor(props){
		super(props);
		this.state = {activeChild:0,navBarOptions:this.props.navOpts};
		this.talkToChildren = this.state.navBarOptions;
		this.changePage = this.props.bodyChangeFunc;
		this.changeTab = this.changeActiveTab.bind(this);
		this.addToMe = this.addToMe.bind(this);
	}
	addToMe(z,fxn){let tempArr = this.talkToChildren;tempArr[z] = fxn;this.talkToChildren=tempArr;};
	changeActiveTab(changeTo){//Change active tab
		if(this.state.activeChild!==changeTo){//only change if trying to change to a different tab
			this.setState({activeChild:changeTo});
			for(var j=0;j<this.state.navBarOptions.length;j++){this.talkToChildren[j](changeTo);}
			this.changePage(changeTo);
		}
	};
	render(){
		let tabs = [];
		for(var i=0;i<this.state.navBarOptions.length;i++){
			tabs.push(<NavItem name={this.state.navBarOptions[i]} key={i} i={i} active={this.state.activeChild} handleClick={this.changeTab} registerChildren={this.addToMe}/>);
		};
		return (<div className="navBar"><div className="navBarLow">{tabs}</div></div>);
	}
}

//Navigation item
class NavItem extends React.Component {
	constructor(props) {
		super(props);
		this.state = {i:this.props.i,name:this.props.name,active:this.props.active};
		this.handleClick = this.props.handleClick;
		this.tellParent = this.tellParent.bind(this);
		this.changeActive = this.changeActive.bind(this);
		this.props.registerChildren(this.state.i,this.changeActive);
	}
	changeActive(changeTo){this.setState({active:changeTo});};
	tellParent(e){if(this.state.active!==this.state.i){this.handleClick(this.state.i);}};
	render(){
		if(this.state.i!==this.state.active){
			return(<span key={this.state.i} className="navItem" onClick={this.tellParent}>{this.state.name}</span>);
		}	return(<span key={this.state.i} className="navItem navActive" onClick={this.tellParent}>{this.state.name}</span>);
	}
}